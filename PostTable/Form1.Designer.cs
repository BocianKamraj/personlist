﻿namespace PostTable
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonAddPerson = new System.Windows.Forms.Button();
            this.ButtonAddPhoto = new System.Windows.Forms.Button();
            this.AgeLabel = new System.Windows.Forms.Label();
            this.nameLabel = new System.Windows.Forms.Label();
            this.AgeTextBox = new System.Windows.Forms.TextBox();
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.ProflePhotoBox = new System.Windows.Forms.PictureBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ((System.ComponentModel.ISupportInitialize)(this.ProflePhotoBox)).BeginInit();
            this.SuspendLayout();
            // 
            // ButtonAddPerson
            // 
            this.ButtonAddPerson.Location = new System.Drawing.Point(501, 317);
            this.ButtonAddPerson.Name = "ButtonAddPerson";
            this.ButtonAddPerson.Size = new System.Drawing.Size(100, 45);
            this.ButtonAddPerson.TabIndex = 0;
            this.ButtonAddPerson.Text = "Confirm";
            this.ButtonAddPerson.UseVisualStyleBackColor = true;
            this.ButtonAddPerson.Click += new System.EventHandler(this.ButtonAddPerson_Click);
            // 
            // ButtonAddPhoto
            // 
            this.ButtonAddPhoto.Location = new System.Drawing.Point(501, 35);
            this.ButtonAddPhoto.Name = "ButtonAddPhoto";
            this.ButtonAddPhoto.Size = new System.Drawing.Size(100, 23);
            this.ButtonAddPhoto.TabIndex = 1;
            this.ButtonAddPhoto.Text = "Select Photo";
            this.ButtonAddPhoto.UseVisualStyleBackColor = true;
            this.ButtonAddPhoto.Click += new System.EventHandler(this.ButtonAddPhoto_Click);
            // 
            // AgeLabel
            // 
            this.AgeLabel.AutoSize = true;
            this.AgeLabel.Location = new System.Drawing.Point(498, 249);
            this.AgeLabel.Name = "AgeLabel";
            this.AgeLabel.Size = new System.Drawing.Size(29, 13);
            this.AgeLabel.TabIndex = 2;
            this.AgeLabel.Text = "Age:";
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(498, 183);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(41, 13);
            this.nameLabel.TabIndex = 3;
            this.nameLabel.Text = "Name: ";
            // 
            // AgeTextBox
            // 
            this.AgeTextBox.Location = new System.Drawing.Point(501, 278);
            this.AgeTextBox.Name = "AgeTextBox";
            this.AgeTextBox.Size = new System.Drawing.Size(100, 20);
            this.AgeTextBox.TabIndex = 4;
            this.AgeTextBox.TextChanged += new System.EventHandler(this.AgeTextBox_TextChanged);
            // 
            // NameTextBox
            // 
            this.NameTextBox.Location = new System.Drawing.Point(501, 208);
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(100, 20);
            this.NameTextBox.TabIndex = 5;
            this.NameTextBox.TextChanged += new System.EventHandler(this.NameTextBox_TextChanged);
            // 
            // ProflePhotoBox
            // 
            this.ProflePhotoBox.Location = new System.Drawing.Point(501, 80);
            this.ProflePhotoBox.Name = "ProflePhotoBox";
            this.ProflePhotoBox.Size = new System.Drawing.Size(100, 81);
            this.ProflePhotoBox.TabIndex = 6;
            this.ProflePhotoBox.TabStop = false;
            this.ProflePhotoBox.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.listView1.Location = new System.Drawing.Point(12, 12);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(249, 350);
            this.listView1.TabIndex = 7;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Name";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Age";
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Photo";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(850, 490);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.ProflePhotoBox);
            this.Controls.Add(this.NameTextBox);
            this.Controls.Add(this.AgeTextBox);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.AgeLabel);
            this.Controls.Add(this.ButtonAddPhoto);
            this.Controls.Add(this.ButtonAddPerson);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.ProflePhotoBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonAddPerson;
        private System.Windows.Forms.Button ButtonAddPhoto;
        private System.Windows.Forms.Label AgeLabel;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.TextBox AgeTextBox;
        private System.Windows.Forms.TextBox NameTextBox;
        private System.Windows.Forms.PictureBox ProflePhotoBox;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
    }
}

