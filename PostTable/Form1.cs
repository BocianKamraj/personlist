﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows.Forms;
using System.IO;

namespace PostTable
{

    public partial class Form1 : Form {

        Bitmap ProfilePhoto;


        ObservableCollection<Person> personList = new ObservableCollection<Person>() {
    //        new Person("Michał", 124,),
    //        new Person("Daniel", 87,)
        };

        public Form1()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void NameTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void AgeTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void ButtonAddPerson_Click(object sender, EventArgs e)
        {
            try
            {
                int Age = int.Parse(AgeTextBox.Text);
                string Name = NameTextBox.Text.ToString();
                personList.Add(new Person(Name, Age, ProfilePhoto));
                MessageBox.Show(personList.Count().ToString());
            }
            catch (System.FormatException)
            {
                MessageBox.Show("Wpisz prawidłowe dane");
            }
        }

        private void ButtonAddPhoto_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";

            if (open.ShowDialog() == DialogResult.OK)
            {
                ProfilePhoto = new Bitmap(open.FileName);
                ProflePhotoBox.Image = new Bitmap(open.FileName);
            }
        }

        public class Person
        {

            public string Name { get; set; }

            public int Age { get; set; }

            public Image Photo { get; set; }

            public Person(string Name, int Age, Bitmap Photo)
            {
                this.Name = Name;
                this.Age = Age;
                this.Photo = Photo;
            }

            ~Person() { }
        }

        private void PersonListView_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
